﻿public class FishAI
{
    /// <summary>
    ///     Name of the fish for identification
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    ///     Min distance in percentage of height
    /// </summary>
    public float MinDistance { get; set; }

    /// <summary>
    ///     Max distance in percentage of height
    /// </summary>
    public float MaxDistance { get; set; }

    /// <summary>
    ///     Min speed in velocity. Lol.
    /// </summary>
    public int MinSpeed { get; set; }

    /// <summary>
    ///     Max speed in velocity. Lol.
    /// </summary>
    public int MaxSpeed { get; set; }

    /// <summary>
    ///     Minimum time for a decision of the fish to be taken when the target height has been reached in seconds
    /// </summary>
    public float DecisionMinimum { get; set; }

    /// <summary>
    ///     Maximum time for a decision of the fish to be taken when the target height has been reached in seconds
    /// </summary>
    public float DecisionMaximum { get; set; }

    public static FishAI[] PossibleFish { get; } = new FishAI[]
    {
    /*
        new FishAI
        {
            Name = "ParryThisYouFilthyCasual",
            MinDistance = .5f,
            MaxDistance = 1f,
            MinSpeed = 200,
            MaxSpeed = 600,
            DecisionMinimum = 0,
            DecisionMaximum = 0.4f,
        }*/
        new FishAI
        {
            Name = "Boring",
            MinDistance = .1f,
            MaxDistance = .5f,
            MinSpeed = 50,
            MaxSpeed = 100,
            DecisionMinimum = 1,
            DecisionMaximum = 2,
        }
    };
}
