using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonRandomPos : MonoBehaviour
{
    [SerializeField]
    List<RectTransform> positions;


    public void Randomize(List<Button> buttons)
    {
        var tempPos = new List<RectTransform>(positions);

        foreach (var button in buttons) 
        { 
            var index = Random.Range(0, tempPos.Count);
            button.transform.position = tempPos[index].transform.position;
            tempPos.RemoveAt(index);
        }
    }
}
