using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private ButtonRandomPos buttonRandom;

    [Header("Menu Buttons")]
    [SerializeField]
    private Button startButton;
    [SerializeField]
    private Button optionsButton;
    [SerializeField]
    private Button quitButton;

    private void Awake()
    {
        RandomizeButtons();
    }

    private void RandomizeButtons()
    {
        var buttons = new List<Button>()
        {
            startButton, optionsButton, quitButton
        };

        buttonRandom.Randomize(buttons);
    }
}
