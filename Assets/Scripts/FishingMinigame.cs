using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FishingMinigame : MonoBehaviour
{
    private enum MinigameState
    {
        None,
        Casting,
        Fishing
    }

    // CannedFish
    public bool CanFish { get; set; } = true;

    [SerializeField]
    public UnityEvent<float> _onCastEvent;

    [SerializeField]
    public UnityEvent<bool> _onCompleted;

    [SerializeField]
    private Transform _castMinigameBase = null;

    [SerializeField]
    private Image _fillSlider = null;

    [SerializeField]
    private Transform _fishingBase = null;

    [SerializeField]
    private Image _fishingCastSlider = null;

    [SerializeField]
    private Gradient _successColorGradient = null;

    [SerializeField]
    private Image _successFillerImage = null;

    [SerializeField]
    private Image _fishImage = null;
    
    private MinigameState _currentState = MinigameState.None;
    private float _currentCastTime = 0;

    private float _fishBarHeight = 0;
    private float _fishCenterHeight = 0;
    private float _currentFishSuccess = 0f;
    
    [SerializeField]
    private float _fishBarAccelleration = 50f;

    private float _fishBarVelocity = 0;

    private FishAI _currentFish;
    private float _fishVelocity = 0;
    private float _fishTargetHeight = 0;
    private bool _fishReachedTarget = false;
    private float _fishWaitTick = 0;
    private float _fishTargetWait = 0;

    private float _fishTimeScale = 5f;

    private void Awake()
    {
        _castMinigameBase.gameObject.SetActive(false);
        _fishingBase.gameObject.SetActive(false);

        var parentFishRect = _fishingCastSlider.transform.parent as RectTransform;
        _fishBarHeight = parentFishRect.rect.height;

        _fishCenterHeight = _fishingCastSlider.rectTransform.rect.height;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!CanFish)
        {
            return;
        }

        switch (_currentState)
        {
            case MinigameState.None:
            {
                if (!Input.GetMouseButtonDown(0))
                {
                    return;
                }

                _currentCastTime = 0;
                _castMinigameBase.gameObject.SetActive(true);
                _currentState = MinigameState.Casting;
                return;
            }
            case MinigameState.Casting:
                HandleMinigameCasting();
                break;
            case MinigameState.Fishing:
                HandleFishingMinigame();
                break;
        }
    }

    private void HandleMinigameCasting()
    {
        _currentCastTime += Time.deltaTime;
        int secondsTime = (int)_currentCastTime;
        float relativizedTime = _currentCastTime - secondsTime;

        _fillSlider.fillAmount = secondsTime % 2 == 0 ? relativizedTime : 1 - relativizedTime;
        if (Input.GetMouseButton(0))
        {
            return;
        }

        _castMinigameBase.gameObject.SetActive(false);
        _fishingBase.gameObject.SetActive(true);
        _fishingCastSlider.rectTransform.localPosition = Vector3.zero;
        _fishImage.rectTransform.localPosition = Vector3.zero;

        _currentFishSuccess = 1 / 3f;
        _currentFish = FishAI.PossibleFish[Random.Range(0, FishAI.PossibleFish.Length)];

        _fishTargetHeight = Random.Range(_currentFish.MinDistance, _currentFish.MaxDistance);
        _fishVelocity = Random.Range(_currentFish.MinSpeed, _currentFish.MaxSpeed);
        _fishReachedTarget = false;

        _currentState = MinigameState.Fishing;
        _onCastEvent?.Invoke(relativizedTime);
    }

    private void HandleFishingMinigame()
    {
        _successFillerImage.fillAmount = _currentFishSuccess;
        _successFillerImage.color = _successColorGradient.Evaluate(_currentFishSuccess);
        
        float targetVelocity = Input.GetMouseButton(0) ? _fishBarAccelleration : -_fishBarAccelleration;
        _fishBarVelocity = Mathf.Lerp(_fishBarVelocity, targetVelocity, 0.005f);

        float currentPosition = _fishingCastSlider.rectTransform.localPosition.y;
        float targetY = currentPosition + _fishBarVelocity * Time.deltaTime * 10;
        if (targetY < 0)
        {
            targetY = 0;
            _fishBarVelocity = -_fishBarVelocity;
        }
        else if (targetY > _fishBarHeight - _fishCenterHeight)
        {
            targetY = _fishBarHeight - _fishCenterHeight;
            _fishBarVelocity = 0;
        }

        _fishingCastSlider.rectTransform.localPosition = new Vector2(0, targetY);
        TickFishAI();

        if (IsOverlappingFish())
        {
            _currentFishSuccess += Time.deltaTime / _fishTimeScale;
        }
        else
        {
            _currentFishSuccess -= Time.deltaTime / _fishTimeScale * 0.66f;
        }

        if (_currentFishSuccess < 0)
        {
            Close(false);
        }
        else if (_currentFishSuccess > 1)
        {
            Close(true);
        }
    }

    private void TickFishAI()
    {
        if (_fishReachedTarget)
        {
            _fishWaitTick += Time.deltaTime;
            if (_fishWaitTick < _fishTargetWait)
            {
                return;
            }

            // Redecide fish AI
            _fishVelocity = Random.Range(_currentFish.MinSpeed, _currentFish.MaxSpeed);
            float targetDistance = Random.Range(_currentFish.MinDistance, _currentFish.MaxDistance);

            int multiplier;
            if (_fishTargetHeight < 0.33)
            {
                multiplier = Random.Range(0, 4) == 0 ? -1 : 1;
            }
            else if (_fishTargetHeight > 0.66)
            {
                multiplier = Random.Range(0, 4) == 0 ? 1 : -1;
            }
            else
            {
                multiplier = Random.Range(0, 2) == 0 ? 1 : -1;
            }
            
            _fishTargetHeight = Mathf.Clamp01(_fishTargetHeight + multiplier * targetDistance);
            _fishReachedTarget = false;
        }

        float currentPosition = _fishImage.rectTransform.localPosition.y;
        float scaledTargetHeight = _fishBarHeight * _fishTargetHeight;
        int directionMultiplier = currentPosition < scaledTargetHeight ? 1 : -1;
        
        currentPosition += _fishVelocity * Time.deltaTime * directionMultiplier;
        if (directionMultiplier == 1 && currentPosition > scaledTargetHeight
            || directionMultiplier == -1 && currentPosition < scaledTargetHeight)
        {
            _fishReachedTarget = true;
            _fishWaitTick = 0;
            _fishTargetWait = Random.Range(_currentFish.DecisionMinimum, _currentFish.DecisionMaximum);
            currentPosition = scaledTargetHeight;
        }

        _fishImage.rectTransform.localPosition = new Vector3(0, currentPosition);
    }

    private bool IsOverlappingFish()
    {
        var fishingRect = _fishingCastSlider.rectTransform.rect;
        float fishingBasePosition = _fishingCastSlider.rectTransform.position.y;

        float fishPosition = _fishImage.rectTransform.position.y;
        return fishingBasePosition <= fishPosition && fishPosition <= fishingBasePosition + fishingRect.height;
    }

    private void Close(bool success)
    {
        _castMinigameBase.gameObject.SetActive(false);
        _fishingBase.gameObject.SetActive(false);

        _currentState = MinigameState.None;
        _onCompleted?.Invoke(success);
    }
}
